﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Interfaces;

namespace ATM_Team5.Models
{
    class Admin:Account,IAdmin
    {
        private static List<Admin> _adminList;
        private bool _status;

        public static List<Admin> AdminList { get => _adminList;}
        public bool Status { get => _status; set => _status = value; }

        /// <summary>
        /// Tạo đối tượng Admin kế thừa từ Acount
        /// </summary>
        /// <param name="password">mật khẩu</param>
        public Admin(string password = "") : base(password:password)
        {
            if (_adminList == null)
            {
                _adminList = new List<Admin>();
            }
            this._status = true;
            _adminList.Add(this);
        }

        


    }
}
