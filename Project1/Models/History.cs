﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Interfaces;
using View;


namespace ATM_Team5.Models
{
    class History:IHistory
    {
        private static List<History> _historyList;
        private static int _autoIncrement = 0;

        private string _id;
        private int _type;
        private int _amount;
        private DateTime _time;
        private Customer _customer;

        public static List<History> HistoryList { get => _historyList; }
        public string Id { get => _id;}
        public int Type { get => _type; set => _type = value; }
        public int Amount { get => _amount; set => _amount = value; }
        public DateTime Time { get => _time; set => _time = value; }
        
        /// <summary>
        /// Tạo đối tượng history để khai báo lịch sử giao dịch
        /// </summary>
        /// <param name="type">loại giao dịch</param>
        /// <param name="amount">số tiền giao dịch</param>
        /// <param name="time">thời gian giao dịch</param>
        /// <param name="customerId">id của khách hàng giao dịch </param>
        public History(int type=0, int amount=0,string time = "",string customerId="")
        {
            if (_historyList == null)
            {
                _historyList = new List<History>();
            }
            var customer = Customer.CustomerList.Find(x => x.Id == customerId);
            if (customer == null)
            {
                throw new Exception("Khong tim thay khach hang");
            }
            this._id = AutoGenId();
            this._type = type;
            this._amount = amount;
            this._time = Convert.ToDateTime(time);

            this._customer = customer;
            this._customer.HistoryList.Add(this);
            _historyList.Add(this);
        }
        private string AutoGenId()
        {
            string id = "";
            int lenghtId = 14;
            int lenghtAutoIncrement = $"{++_autoIncrement}".Length;
            int lenghtAutoGen = lenghtId - lenghtAutoIncrement;

            for (int i = 0; i < lenghtAutoGen; i++)
            {
                id += "0";
            }

            id += $"{_autoIncrement}";

            return id;
        }

        public string[] ToFileView()
        {
            List<string> result = new List<string>();

            result.Add($"Ma Khach Hang: {this._customer.Id}");
            result.Add($"Ten Khach Hang: {this._customer.Name}");
            result.Add($"Loai giao dich: {this._type}");
            if (this.Type == 1)
            {
                result.Add($"ID nguoi Nhan: {this._customer}");
                result.Add($"Ten nguoi Nhan: {this._customer.Name}");
            }
            result.Add($"So Tien Giao Dich: {this._amount}");
            result.Add($"Thoi gian giao dich: {this._time}");
            return result.ToArray();
        }

        public void WriteFileView()
        {
            string link = $"History/${this._id}.txt";
            string[] data = this.ToFileView();
            IOFIle.WriteFile(link, data);
        }
    }
}
