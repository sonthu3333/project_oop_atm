﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Interfaces;
using View;


namespace ATM_Team5.Models
{
    class Customer:ICustomer
    {
        private static List<Customer> _customerList;
        private static int _autoIncrement = 0;
        private string _id;
        private string _name;
        private int _balance;
        private string _moneyType;
        private List<History> _historyList;
        private List<Card> _cardList;

        public static List<Customer> CustomerList { get => _customerList;}
        public string Id { get => _id;}
        public string Name { get => _name; set => _name = value; }
        public int Balance { get => _balance; set => _balance = value; }
        public string Moneytype { get => _moneyType; set => _moneyType = value; }
        public List<History> HistoryList { get => _historyList;}
        public List<Card> CardList { get => _cardList; }


        /// <summary>
        /// Tạo đối tượng Customer để quản lý thông tin customer , list card , list history 
        /// </summary>
        /// <param name="name">tên</param>
        /// <param name="balance">số dư</param>
        /// <param name="moneytype">loại tiền</param>
        public Customer(string name="", int balance=0,string moneytype="")
        {
            if( String.IsNullOrEmpty(name))
            {
                throw new Exception("Ten khong duoc de trong");
            }
            if (balance < 0)
            {
                throw new Exception("Tien khong duoc am");
            }
            if (String.IsNullOrEmpty(moneytype))
            {
                throw new Exception();
            }
            if (_customerList == null)
            {
                _customerList = new List<Customer>();
            }
            this._id = AutoGenId();
            this._name = name;
            this._balance = balance;
            this._moneyType = moneytype;
            this._cardList = new List<Card>();
            this._historyList = new List<History>();
            _customerList.Add(this);
        }
        private string AutoGenId()
        {
            string id = "";
            int lenghtId = 14;
            int lenghtAutoIncrement = $"{++_autoIncrement}".Length;
            int lenghtAutoGen = lenghtId - lenghtAutoIncrement;

            for (int i = 0; i < lenghtAutoGen; i++)
            {
                id += "0";
            }

            id += $"{_autoIncrement}";

            return id;
        }

        public string[] ToFileView()
        {
            List<string> result = new List<string>();

            result.Add($"ID: {this._id}");
            result.Add($"Ten Nguoi Dung: {this._name}");
            result.Add($"So Du: {this._balance}");
            result.Add($"Loai Tien: {this._moneyType}");

            return result.ToArray();
        }

        public void WriteFileView()
        {
            string link = $"ID/${this._id}.txt";
            string[] data = this.ToFileView();
            IOFIle.WriteFile(link, data);
        }
    }
}
