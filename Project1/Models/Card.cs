﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Interfaces;
using View;

namespace ATM_Team5.Models
{
    class Card:Account,ICard
    {
        private static List<Card> _cardsList;
        private Customer _customer;
        private bool _status;
        public static List<Card> CardsList { get => _cardsList; }


        internal Customer Customer { get => _customer;}
        public bool Status { get => _status; set => _status = value; }



        /// <summary>
        /// tạo đối tượng card kế thừa từ Acount
        /// tạo đối tượng _customer để quản lý card
        /// </summary>
        /// <param name="password">mật khẩu</param>
        /// <param name="customerId">id cua khach hang</param>
        public Card(string password = "",string customerId = "") : base(password: password)
        {
            var customer = Customer.CustomerList.Find(x => x.Id == customerId);
            if (customer == null)
            {
                throw new Exception("Khong tim thay khach hang");
            }

            if (_cardsList == null)
            {
                _cardsList = new List<Card>();
            }

            this._customer = customer;
            this._status = true;
            this._customer.CardList.Add(this);
            _cardsList.Add(this);
        }

        public string[] ToFileView()
        {
            List<string> result = new List<string>();

            result.Add($"Ma the: {this._id}");
            result.Add($"Ma khach hang: {this.Customer.Id}");
            result.Add($"Ten khach hang: {this.Customer.Name}");
            result.Add($"So du tai khoan: {this.Customer.Balance}");
            result.Add($"Loai tien: {this.Customer.Moneytype}");
            result.Add($"Trang thai the: {base.Status}");

            return result.ToArray();
        }

        public void WriteFileView()
        {
            string link = $"Cards/${this._id}.txt";
            string[] data = this.ToFileView();
            IOFIle.WriteFile(link,data);
        }
    }
}
