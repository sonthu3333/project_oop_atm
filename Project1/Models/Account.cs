﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Interfaces;

namespace ATM_Team5.Models
{
    class Account:IAcount
    {

        private static int _autoIncrement = 0;
        private static List<Account> _acountsList;
        private bool _status;

        protected string _id;
        protected string _password;


        internal static List<Account> AcountsList { get => _acountsList;  }
        public string Id { get => _id;}
        public string Password { get => _password; set => _password = value; }
        public bool Status { get => _status; set => _status = value; }

        /// <summary>
        /// Là lớp cha , để lớp Admin và Card kế thừa _id và _password
        /// _id tạo tự động
        /// </summary>
        /// <param name="password">mật khẩu</param>
        protected Account(string password = "",int? id = null)
        {
            if (password.Length != 6)
            {
                throw new Exception("mat khau khong dung dinh dang");
            }
            if (_acountsList == null)
            {
                _acountsList = new List<Account>();
            }
            this._password = password;
            this._id = AutoGenId();
            this._status = true;

            _acountsList.Add(this);
        }

       

        private string AutoGenId()
        {
            string id = "";
            int lenghtId = 14;
            int lenghtAutoIncrement = $"{++_autoIncrement}".Length;
            int lenghtAutoGen = lenghtId - lenghtAutoIncrement;

            for (int i = 0; i < lenghtAutoGen; i++)
            {
                id += "0";
            }

            id += $"{_autoIncrement}";

            return id;
        }

        public static string[] ToFile()
        {
            List<string> dataAccount = new List<string>();
            dataAccount.Add($"{_autoIncrement}");
            if (_acountsList != null)
            {
                for (int i = 0; i < _acountsList.Count; i++)
                {
                    dataAccount.Add($"{_acountsList[i].Id}###{_acountsList[i].Password}");
                }
            }

            return dataAccount.ToArray();
        }
    }
}
