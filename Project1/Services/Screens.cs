﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Models;

namespace ATM_Team5.Services
{
    static class Screens
    {
        private static List<Customer> _fakeCustomerList = new List<Customer>()
        {
            new Customer(name:"son",balance:100000,moneytype:"VND"),
            new Customer(name:"hieu",balance:200000,moneytype:"VND"),
            new Customer(name:"quan",balance:1000000,moneytype:"VND"),
        };
        public static List<Account> _fakeAcountList = new List<Account>()
        {
            new Admin("admin1"),
                new Admin("admin2"),
                new Admin("admin3"),
                new Card("123456","00000000000001"), 
                new Card("123457","00000000000001"),
                new Card("123458","00000000000002"),
                new Card("123459","00000000000002"),
                new Card("123450","00000000000003"),
                new Card("123455","00000000000003"),
        };
        //private static List<Card> _fakeCardList = new List<Card>()
        //{
        //        new Card("123456","00000000000001"),
        //        new Card("123457","00000000000001"),
        //        new Card("123458","00000000000002"),
        //        new Card("123459","00000000000002"),
        //        new Card("123450","00000000000003"),
        //        new Card("123455","00000000000003"),
        //};
        private static void AdminMenu()
        {
            // can lay du lieu thi dictionary , khong thi list bth
            View.HeaderTitle("Menu");
            char chosen;
            List<IKeyValue> _menuList = new List<IKeyValue>();
            _menuList.Add(new KeyValue<string>(key: "1", value: "Xem danh sach tai khoan"));
            _menuList.Add(new KeyValue<string>(key: "2", value: "Them tai khoan"));
            _menuList.Add(new KeyValue<string>(key: "3", value: "Xoa tai khoan"));
            _menuList.Add(new KeyValue<string>(key: "4", value: "Mo khoa tai khoan"));
            _menuList.Add(new KeyValue<string>(key: "5", value: "Dang xuat"));
            _menuList.Add(new KeyValue<string>(key: "6", value: "Thoat"));
            View.WriteLines(_menuList);

            Dictionary<string, IKeyValue> select = new Dictionary<string, IKeyValue>();
            select.Add(key: "chosen", new KeyValue<char>(key: "Nhap lua chon"));
            View.ReadLines(select);

            chosen = (char)select["chosen"].GetValue();
            do
            {
                switch (chosen)
                {
                    case '1':
                        DisplayListCard();
                        break;
                    case '2':
                        AddAcount();
                        break;
                    case '3':
                        DeleteAcount();
                        break;
                    case '4':
                        UnlockAcount();
                        break;
                    case '5':
                        Logout();
                        break;
                    case '6':
                        Exit();
                        break;
                }
            } while (chosen != '1' && select.Keys.Count != '2' && chosen != '3' && chosen != '4' && chosen != '5' && chosen != '6' && chosen != (char)ConsoleKey.Escape);
        }
        private static void CustomerMenu()
        {
            View.HeaderTitle("Menu");
            char chosen;
            List<IKeyValue> _menuList = new List<IKeyValue>();
            _menuList.Add(new KeyValue<string>(key: "1", value: "Xem thong tin tai khoan"));
            _menuList.Add(new KeyValue<string>(key: "2", value: "Rut tien"));
            _menuList.Add(new KeyValue<string>(key: "3", value: "Chuyen tien"));
            _menuList.Add(new KeyValue<string>(key: "4", value: "Xem noi dung giao dich"));
            _menuList.Add(new KeyValue<string>(key: "5", value: "Doi ma pin"));
            _menuList.Add(new KeyValue<string>(key: "6", value: "Dang xuat"));
            _menuList.Add(new KeyValue<string>(key: "7", value: "Thoat"));
            View.WriteLines(_menuList);

            Dictionary<string, IKeyValue> select = new Dictionary<string, IKeyValue>();
            select.Add(key: "chosen", new KeyValue<char>(key: "Moi ban chon"));
            View.ReadLines(select);
            chosen = (char)select["chosen"].GetValue();


            do
            {
                switch (chosen)
                {
                    case '1':
                        SeeInformation();
                        break;
                    case '2':
                        Withdraw();
                        break;
                    case '3':
                        Tranfer();
                        break;
                    case '4':
                        DisplayListHistory();
                        break;
                    case '5':
                        ChangePassword();
                        break;
                    case '6':
                        Logout();
                        break;
                    case '7':
                        ExitCustomer();
                        break;
                }
            } while (chosen != '1' && chosen != '2' && chosen != '3' && chosen != '4' && chosen != '5' && chosen != '6' && chosen != '6' && chosen != (char)ConsoleKey.Escape);
        }
        public static void Login()
        {
            var x = _fakeAcountList;
            //tieu de
            View.HeaderTitle("Dang Nhap");
            //
            Dictionary<string, IKeyValue> _loginList = new Dictionary<string, IKeyValue>();
            _loginList.Add(key: "username", new KeyValue<string>(key: "Ten dang nhap", value: string.Empty));
            _loginList.Add(key: "password", new KeyValue<string>(key: "Mat khau", value: string.Empty, isPassword: true));

            Account subject;
            
            bool checklogin;
            do
            {
                View.ReadLines(_loginList);
                string username = (string)_loginList["username"].GetValue();
                string password = (string)_loginList["password"].GetValue();
                //Goi ham check login tu function
                subject = Functions.CheckLogin(username,password);
                if (subject != null)
                {
                    checklogin = true;
                }
                else
                {
                    checklogin = false;
                    //kiem tra pass qua 3 lan
                    List<string> _accountWrong = new List<string>();
                    _accountWrong.Add(password);
                    if (_accountWrong.Count == 3)
                    {
                        //goi ham lockAccount tu funcion
                        bool checkLock = Functions.LockAccount(username);
                        if (checkLock == true)
                        {
                            View.Warning("Tai khoan bi khoa");
                            _accountWrong = new List<string>();
                        }
                        else
                        {
                            View.Warning("Tai khoan hoac mat khau khong dung");
                        }
                    }
                    else
                    {
                        _accountWrong = new List<string>();
                    }
                }
            } while (checklogin == false);

            // phan biet admin hoac customer
            if (subject is Card)
            {
                CustomerMenu();
            }
            else if (subject is Admin)
            {
                AdminMenu();
            }
        }
        //Admin menu
        private static void DisplayListCard()
        {
            //Goi ham xuat list card
            List<Card> _cardList = Functions.GetListCard();
            Console.Clear();
            List<IKeyValue> _displayList = new List<IKeyValue>();
            for (int i = 0; i < _cardList.Count; i++)
            {
                _displayList.Add(new KeyValue<string>(key: "*****************************", value: "*****************************"));
                _displayList.Add(new KeyValue<string>(key: "username", value: _cardList[i].Id));
                _displayList.Add(new KeyValue<string>(key: "custommername", value: _cardList[i].Customer.Name));
            }
            View.WriteLines(_displayList);
            Console.ReadKey();
            AdminMenu();
        }
        private static void AddAcount()
        {
            
            //Ham them Account
            Dictionary<string, IKeyValue> _addAcount = new Dictionary<string, IKeyValue>();
            _addAcount.Add(key: "customerId", new KeyValue<string>(key: "Nhap id khach hang: ", value: string.Empty));
            View.ReadLines(_addAcount);
            string customerId = (string)_addAcount["customerId"].GetValue();
            //addAcount((string)_addAcount[customerId].GetValue())
            bool _testham=Functions.AddAccount(customerId);
            try
            {
                if (_testham == true)
                {
                    View.Sucesss("Them thanh cong");
                }
                else
                {
                    View.Warning($"Khong tim thay khach hang co id: {customerId}");
                }
            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            AdminMenu();
        }
        private static void DeleteAcount()
        {
            //Ham them Account
            Dictionary<string, IKeyValue> _deleteAcount = new Dictionary<string, IKeyValue>();
            _deleteAcount.Add(key: "cardId", new KeyValue<string>(key: "Nhap id the can xoa: ", value: string.Empty));
            View.ReadLines(_deleteAcount);
            string id = (string)_deleteAcount["cardId"].GetValue();
            //deleteAcount((string)_deleteAcount[cardId].GetValue())
            try
            {
                if (Functions.DeleteCardByID(id)==true)
                {
                    View.Sucesss("Xoa thanh cong");
                }
                else
                {
                    View.Warning("Xoa that bai");
                }

            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            AdminMenu();
        }
        private static void UnlockAcount()
        {
            //Ham mo khoa Account
            Dictionary<string, IKeyValue> _unlockAcount = new Dictionary<string, IKeyValue>();
            _unlockAcount.Add(key: "cardId", new KeyValue<string>(key: "Nhap id the mo khoa: ", value: string.Empty));
            View.ReadLines(_unlockAcount);
            string id = (string)_unlockAcount["cardId"].GetValue();
            //deleteAcount((string)_deleteAcount[cardId].GetValue())
            try
            {
                if (Functions.UnlockAccount(id) == true)
                {
                    View.Sucesss("Mo khoa thanh cong");
                }
                else
                {
                    View.Warning("tai khoan nay khong bi khoa hoac khong ton tai tai khoan nay");
                }

            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            AdminMenu();

        }
        private static void Logout()
        {
            Login();
        }
        private static void Exit()
        {
            return;
        }
        //Customer menu
        private static void SeeInformation()
        {
            //Functions.ViewAccountInformation();
            Customer customer = _fakeCustomerList[1];
            List<IKeyValue> _menuList = new List<IKeyValue>();
            _menuList.Add(new KeyValue<string>(key: "Id", value: customer.Id));
            _menuList.Add(new KeyValue<string>(key: "Ten", value: customer.Name));
            _menuList.Add(new KeyValue<string>(key: "So du", value: $"{customer.Balance}"));
            _menuList.Add(new KeyValue<string>(key: "Loai tien", value: customer.Moneytype));
            View.WriteLines(_menuList);
            CustomerMenu();
        }
        private static void Withdraw()
        {
            //Ham rut tien
            Dictionary<string, IKeyValue> _withdraw = new Dictionary<string, IKeyValue>();
            _withdraw.Add(key: "money", new KeyValue<string>(key: "Nhap so tien muon rut: ", value: string.Empty));
            View.ReadLines(_withdraw);
            int money = (int)_withdraw["money"].GetValue();
            try
            {
                if (Functions.WithdrawMoney(money)==true)
                {
                    View.Sucesss("Rut tien thanh cong");
                }
                else
                {
                    View.Warning("So du khong du de rut tien");
                }

            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            CustomerMenu();
        }
        private static void Tranfer()
        {
            //Ham chuyen tien
            Dictionary<string, IKeyValue> _addAcount = new Dictionary<string, IKeyValue>();
            _addAcount.Add(key: "customerId", new KeyValue<string>(key: "Nhap id cua nguoi nhan tien: ", value: string.Empty));
            _addAcount.Add(key: "money", new KeyValue<string>(key: "so tien muon gui ", value:string.Empty));
            View.ReadLines(_addAcount);
            string customerId = (string)_addAcount["customerId"].GetValue();
            int money = (int)_addAcount["money"].GetValue();
            try
            {
                if (Functions.Transfers(customerId,money) == true)
                {
                    View.Sucesss("Chuyen tien thanh cong");
                }
                else
                {
                    View.Warning($"Khong tim thay khach hang co id: {customerId}");
                }
            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            CustomerMenu();
        }
        private static void DisplayListHistory()
        {
            //Goi ham xuat list card
            List<History> _historyList = Functions.ViewTransactionContent();
            Console.Clear();
            List<IKeyValue> _displayList = new List<IKeyValue>();
            for (int i = 0; i < _historyList.Count; i++)
            {
                _displayList.Add(new KeyValue<string>(key: "*****************************", value: "*****************************"));
                _displayList.Add(new KeyValue<string>(key: "Id giao dich", value: _historyList[i].Id));
                _displayList.Add(new KeyValue<string>(key: "Loai giao dich", value: $"{_historyList[i].Type}"));
                _displayList.Add(new KeyValue<string>(key: "So tien giao dich", value: $"{_historyList[i].Amount}"));
                _displayList.Add(new KeyValue<string>(key: "Thoi gian giao dich", value: ($"{_historyList[i].Time}")));
            }
            View.WriteLines(_displayList);
            Console.ReadKey();
            CustomerMenu();
        }
        private static void ChangePassword()
        {
            //Ham doi password
            Dictionary<string, IKeyValue> _addAcount = new Dictionary<string, IKeyValue>();
            _addAcount.Add(key: "newpass", new KeyValue<string>(key: "Nhap mat khau moi: ", value: string.Empty));
            View.ReadLines(_addAcount);
            string newpass = (string)_addAcount["newpass"].GetValue();
            try
            {
                Functions.ChangePIN(newpass);
                View.Sucesss("Doi mat khau thanh cong");
            }
            catch (Exception e)
            {
                View.Warning(e.Message);
            }
            Console.ReadKey();
            CustomerMenu();
        }
        private static void LogoutCustomer()
        {
            Login();
        }
        private static void ExitCustomer()
        {
            return;
        }
    }
}
