﻿using System;
using System.Collections.Generic;
using System.Text;
using ATM_Team5.Models;
namespace ATM_Team5.Services
{
    static class Functions
    {
        //chức năng của admin
        public static Account userlogin;

        internal static void GetAllData()
        {

        }

        internal static void SaveAllData()
        {

        }

        /// <summary>
        /// hàm kiểm tra xem có đúng id mật khẩu không
        /// </summary>
        /// <param name="id">id đăng nhập</param>
        /// <param name="password">mật khẩu đăng nhập</param>
        /// <returns>thành công hoặc thất bại</returns>
        internal static Account CheckLogin(string username, string password)
        {
            Account user = Account.AcountsList.Find(item => item.Id == username);
            if(user is Admin)
            {
                userlogin = user;
                if (((Admin)userlogin).Password == password && ((Admin)userlogin).Status == true)
                {
                    return user;
                }
                else return null;
                
            }else if(user is Card)
            {
                if (((Card)user).Status == true && user.Password == password)
                {
                    userlogin = user;
                    if (((Card)userlogin).Password==password && ((Card)userlogin).Status==true)
                    {
                        return user;
                    }
                    else return null;
                }
            }
            return null;
        }
        /// <summary>
        /// 1. Hàm xem danh sách Tài khoản: Hiển thị toàn bộ thông tin danh sách TheTu có trong file TheTu.txt.
        /// </summary>
        /// <returns>in danh sách toàn bộ thẻ từ trong file TheTu</returns>        

        internal static List<Card> GetListCard()
        {
            return Card.CardsList;
        }

        /// <summary>
        /// thêm Account
        /// </summary>
        /// <param name="id"></param>
        /// <returns>true:thêm thành công - false:thêm thất bại</returns>
        internal static bool AddAccount(string id )
        {
            if (Customer.CustomerList.Find(item => item.Id == id) != null)
            {
                Card acc = new Card("123456",id);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Xóa một Tài khoản theo ID
        /// </summary>
        /// <param name="id">truyền vào id để xóa</param>
        /// <returns>return true nếu xóa thành công hông thì return false</returns>
        internal static bool DeleteCardByID(string idcard)
        {
            Card card = Card.CardsList.Find(item => item.Id == idcard);
            if (card != null)
            {
                card.Customer.CardList.Remove(card);
                Account.AcountsList.Remove(card);
                Card.CardsList.Remove(card);
                return true;
            }
            return false;
        }


        /// <summary>
        /// d.	Mở khóa Tài Khoản: cho phép mở khóa theo ID những tài khoản đã bị khóa do đăng nhập sai quá 3 lần.
        /// </summary>
        /// <returns>return true nếu xóa thành công hông thì return false</returns>
        internal static bool UnlockAccount(string id)
        {
            var tk = Account.AcountsList.Find(item => item.Id == id);
            if (tk is Card)
            {
                var card = (Card)tk;
                if (card.Status == false)
                {
                    card.Status = true;
                    return true;
                }
            }
            return false;
        }



        //chắc năng của users

        /// <summary>
        /// khóa tài khoản do nhập sai quá 3 lần
        /// </summary>
        /// <returns>khóa thành công</returns>
        internal static bool LockAccount(string id)
        {
            var tk = Account.AcountsList.Find(item => item.Id == id);
            if (tk is Card)
            {
                var card = (Card)tk;
                if (card.Status == true)
                {
                    card.Status = false;
                    return true;
                }
            }
            if (tk is Admin)
            {
                var admin = (Admin)tk;
                if (admin.Status == true)
                {
                    admin.Status = false;
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Xem thông tin tài khoản: Cho phép người dùng xem thông tin và số dư tài khoản của mình từ file [ID].txt.
        /// </summary>
        /// <param name="id">truyền vào id</param>
        /// <returns>in ra thông tin và số dư tài khoản</returns>
        internal static Customer ViewAccountInformation()
        {
            return ((Card)userlogin).Customer;
        }

        /// <summary>
        /// Rút tiền bằng cách nhập vào số tiền cần rút và lưu lại thông tin giao dịch của user
        /// </summary>
        /// <param name="tienrut">Nhập vào số tiền cần rút</param>
        /// <returns>rút tiền thành công hay thất bại</returns>
        internal static bool WithdrawMoney(int tienrut)
        {
            int balance = ((Card)userlogin).Customer.Balance;
            if (balance >= tienrut)
            {
                ((Card)userlogin).Customer.Balance -= tienrut;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Chuyển tiền bằng cách nhập vào tài khoản nhận tiền và số tiền cần chuyển rồi lại lưu thông tin giao dịch của user.
        /// </summary>
        /// <param name="tknhantien">nhập tài khoản cần sẽ nhận tiền được chuyển</param>
        /// <param name="tienchuyen">nhập số tiền sẽ chuyển</param>
        /// <returns></returns>
        internal static bool Transfers(string tknhantien, int tienchuyen)
        {
            var tk = Customer.CustomerList.Find(item => item.Id == tknhantien);
            if (tk != null)
            {
                ((Card)userlogin).Customer.Balance += tienchuyen;
                return true;
            }
            return false;
        }

        /// <summary>
        /// Cho phép người dùng xem lại toàn bộ giao dịch của mình. 
        /// </summary>
        /// <returns>in ra toàn bộ giao dịch của tài khoản</returns>
        internal static List<History> ViewTransactionContent()
        {
            return ((Card)userlogin).Customer.HistoryList;
        }


        /// <summary>
        /// e.	Đổi mã Pin
        /// </summary>
        /// <param name="mkmoi">nhập mã pin mới</param>
        /// <returns>thành công đổi mã pin mới nếu nhập đúng</returns>
        internal static bool ChangePIN(string mkmoi)
        {
            userlogin.Password = mkmoi;
            return true;
        }
    }
}
